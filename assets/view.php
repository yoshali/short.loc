<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/form.js"></script>
    <title>Get Link</title>
</head>
<body>
    <div class="container jumbotron">
        <h1 class="display-4">Get the short link!</h1>
        <p class="lead">This is a simple service for getting short links. Enter your link here and press the bottom to get new link.</p>
        <hr class="my-4">

        <form action="/main/form" method="post">

            <div class="form-group">
                <label>Your link</label>
                <input type="text" name="link" placeholder="Enter the link">
            </div>

            <p class="lead">
                <button class="btn btn-primary btn-lg btn-dark" name="enter" type="submit">Submit</button>
            </p>
        </form>

        <div id="result"></div>
    </div>
</body>
</html>
