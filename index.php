<?php

require_once 'libs/Dev.php';
require_once 'libs/helper.php';

use handlers\Router;

spl_autoload_register(function ($class) {
    $path = str_replace('\\', '/', $class.'.php');
    if(file_exists($path)){
        require $path;
    }
});

session_start();

$router = new Router;
$router->run();