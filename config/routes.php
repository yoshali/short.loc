<?php
return array(

    '' => [
    'handler' => 'main',
    'action' => 'index',
    ],

    'main/form' => [
        'handler' => 'main',
        'action' => 'form',
    ],

    '([A-Z,a-z]+)' => [
    'handler' => 'router',
    'action' => 'redirect',
    ]
);