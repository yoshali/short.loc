create table maps(
    id int(5) not null auto_increment,
    long_link varchar(50),
    short_link varchar(50),
    primary key (id),
    unique (long_link),
    unique (short_link)
);
