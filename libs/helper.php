<?php
/*
 *generation of random string by the number of characters
 *get the number of characters
 *return string
 */
function get_random($num){
    $arr = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
           'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    $string = '';

    for($i = 0; $i < $num; $i++){
        $string .= $arr[random_int(0,count($arr)-1)];
    }
    return $string;
}