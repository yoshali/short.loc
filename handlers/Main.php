<?php


namespace handlers;

use handlers\Db;


class Main
{

    public function indexAction()
    {
        require 'assets/view.php';
    }

    public function formAction()
    {
        //Check if the post data is empty.
        if ($_POST['link'] == '') {
            echo 'Enter the link';
        } else {

            //Get all data from the db.
            $link = $_POST['link'];
            $db = new Db();
            $rows = $db->row('select * from maps');

            //Check if the link exists.
            foreach ($rows as $row) {
                if ($link == $row['long_link']) {

                    //Return exists value.
                    echo 'Your link: ' . $_SERVER['HTTP_REFERER'] . $row['short_link'];
                    exit;
                }
            }

            //Well, there is no short link in the database. Then, create it!
            //We just need short value of query string in database for flexibility of application

            $num = get_random(5);
            $flag = false;

            while(!$flag){
                $key = array_search($num, array_column($rows, 'short_link'));
                if($flag === $key){
                    $flag = true;
                } else{
                    $num = get_random(5);
                }
            }


            $result = $db->query(
                'insert into maps (long_link, short_link) values (:link, :num)',
                $params = [
                    'link' => $link,
                    'num' => $num
                ]);


            echo $_SERVER['HTTP_REFERER'] . $num;
        }
    }
}