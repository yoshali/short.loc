<?php

namespace handlers;

class Router
{
    private $routes = [];
    private $params = [];

    function __construct()
    {
        $arr = require 'config/routes.php';
        foreach($arr as $key => $val){
            $this->add($key, $val);
        }
    }

    public function add($route, $params)
    {
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }

    public function match()
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');
        foreach ($this->routes as $route => $params){
            if(preg_match($route, $url, $matches)){
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        if ($this->match())
        {
            $path = 'handlers\\'.ucfirst($this->params['handler']);
            if (class_exists($path))
            {
                $action = $this->params['action'].'Action';
                if(method_exists($path, $action))
                {
                    $controller = new $path($this->params);
                    $controller->$action();
                }else{
                    $this->errorCode();
                }
            }else{
                $this->errorCode();
            }
        }else{
            $this->errorCode();
        }
    }

    public function redirectAction()
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');

        //Get all data from the db.
        $db = new Db();
        $rows = $db->row('select * from maps');

        //Check if the link exists.
        foreach ($rows as $row) {
            if ($url == $row['short_link']) {

                //Redirect to exists link.
                header('Location: ' . $row['long_link']);
                exit;
            }
        }

        $this->errorCode();

    }

    public static function errorCode()
    {
        http_response_code(404);
        require 'assets/404.php';
    }
}